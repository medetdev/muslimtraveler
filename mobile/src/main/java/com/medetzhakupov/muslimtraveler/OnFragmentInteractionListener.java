package com.medetzhakupov.muslimtraveler;

import android.net.Uri;

/**
 * Created by FahmiAnuar on 3/4/2015.
 */
public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    public void onFragmentInteraction(Uri uri);
}
