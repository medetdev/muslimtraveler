package com.medetzhakupov.muslimtraveler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medetzhakupov.muslimtraveler.model.Place;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;

import java.util.List;

/**
 * Created by mac on 4/22/15.
 */

public class EndlessRecyclerViewAdapter extends RecyclerView.Adapter<EndlessRecyclerViewAdapter.ViewHolder> {

    private ParseQueryAdapter<Place> parseAdapter;

    private ViewGroup parseParent;

    private EndlessRecyclerViewAdapter endlessAdapter = this;

    public EndlessRecyclerViewAdapter(Context context, ViewGroup parentIn) {
        parseParent = parentIn;
        parseAdapter = new ParseQueryAdapter<Place>(context, Place.class) {
            @Override
            public View getItemView(Place object, View v, ViewGroup parent) {
                if (v == null) {
                    v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw, parent, false);
                }
                super.getItemView(object, v, parent);


                return v;
            }


        };
        parseAdapter.addOnQueryLoadListener(new OnQueryLoadListener());
        parseAdapter.loadObjects();
    }

    @Override
    public EndlessRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        parseAdapter.getView(position, holder.cardView, parseParent);
    }

    @Override
    public int getItemCount() {
        return parseAdapter.getCount();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View cardView;

        public ViewHolder(View v) {
            super(v);
            cardView = v;
        }
    }

    public class OnQueryLoadListener implements ParseQueryAdapter.OnQueryLoadListener<Place> {

        public void onLoading() {

        }

        @Override
        public void onLoaded(List<Place> places, Exception e) {
            endlessAdapter.notifyDataSetChanged();
        }
    }
}
