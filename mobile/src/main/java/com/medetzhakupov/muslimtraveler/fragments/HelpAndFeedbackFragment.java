package com.medetzhakupov.muslimtraveler.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.activities.FeedbackActivity;
import com.medetzhakupov.muslimtraveler.activities.HelpActivity;

public class HelpAndFeedbackFragment extends Fragment {
    public static final String FRAGMENT_TAG = "HelpAndFeedbackFragment";
    private ListView mListView;

    public static HelpAndFeedbackFragment newInstance() {
        HelpAndFeedbackFragment fragment = new HelpAndFeedbackFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public HelpAndFeedbackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_help_and_feedback, container, false);
        mListView = (ListView)view.findViewById(R.id.list_view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final String[] strings = new String[10];
        for (int i = 0; i < 10; i++)
            strings[i] = "Help" + i;
        final MyAdapter adapter = new MyAdapter(getActivity(), strings);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(view.getContext(), strings[i], Toast.LENGTH_SHORT).show();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (adapter.getItemViewType(i) == MyAdapter.FEEDBACK_TYPE){
                    startActivity(new Intent(getActivity(), FeedbackActivity.class));
                } else {
                    startActivity(new Intent(getActivity(), HelpActivity.class));
                }
            }
        });
    }

    class MyAdapter extends BaseAdapter
    {
        public final static int FEEDBACK_TYPE = 0;
        public final static int HELP_TYPE = 1;

        Context mContext;
        String[] items;
        public MyAdapter(Context context, String[] strings){
            mContext = context;
            items = strings;
        }

        @Override
        public int getCount() {
            if (items == null)
                return 0;
            return items.length;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (getItemViewType(i) == FEEDBACK_TYPE){
                view = LayoutInflater.from(mContext).inflate(R.layout.feedback_list_item, viewGroup, false);
//                getListView().setDividerHeight(0);
//                getListView().setDivider(null);
            } else {
                view = LayoutInflater.from(mContext).inflate(R.layout.help_list_item, viewGroup, false);
                TextView textView = (TextView)view.findViewById(R.id.text_view1);
                textView.setText(items[i]);
            }

            return view;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == FEEDBACK_TYPE)
                return FEEDBACK_TYPE;
            else
                return HELP_TYPE;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Object getItem(int i) {
            return items[i];
        }
    }
}
