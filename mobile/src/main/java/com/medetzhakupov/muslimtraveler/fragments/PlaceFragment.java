package com.medetzhakupov.muslimtraveler.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.medetzhakupov.muslimtraveler.MyApplication;
import com.medetzhakupov.muslimtraveler.Utilities;
import com.medetzhakupov.muslimtraveler.activities.DetailActivity;
import com.medetzhakupov.muslimtraveler.adapters.PlaceAdapter;
import com.medetzhakupov.muslimtraveler.components.EndlessRecyclerOnScrollListener;
import com.medetzhakupov.muslimtraveler.OnFragmentInteractionListener;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.components.ParseProxyObject;
import com.medetzhakupov.muslimtraveler.model.Photo;
import com.medetzhakupov.muslimtraveler.model.Place;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link PlaceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PlaceFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    private View mProgressContainer, mContainer;
    private TextView mEmtyView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private PlaceAdapter mAdapter;
    private GridLayoutManager mLayoutManager;
    private ObservableRecyclerView recyclerView;
    EndlessRecyclerOnScrollListener endlessScrollListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PlaceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PlaceFragment newInstance() {
        PlaceFragment fragment = new PlaceFragment();
        return fragment;
    }

    public PlaceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place, container, false);
        mProgressContainer = view.findViewById(R.id.progress_container);
        mContainer = view.findViewById(R.id.container);
        mEmtyView = (TextView)view.findViewById(R.id.empty_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                initAdapter();
            }
        });

        recyclerView = (ObservableRecyclerView) view.findViewById(R.id.scrollable);
        mLayoutManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.number_of_rows));
        recyclerView.setLayoutManager(mLayoutManager);
        setContainerShown(false, false);
        initAdapter();

        Fragment parentFragment = getParentFragment();
        ViewGroup viewGroup = (ViewGroup) parentFragment.getView();
        if (viewGroup != null) {
            recyclerView.setTouchInterceptionViewGroup((ViewGroup) viewGroup.findViewById(R.id.container));
            if (parentFragment instanceof ObservableScrollViewCallbacks) {
                recyclerView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentFragment);
            }
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (endlessScrollListener != null)
            endlessScrollListener.reset(0, true);
    }

    public void initAdapter()
    {
        if (mAdapter != null)
            mAdapter = null;
        mAdapter = new PlaceAdapter(getActivity(), recyclerView, new PlaceAdapter.ILoader() {
            @Override
            public void onLoadFinish(int currentItemsCount, int newItemsCount) {
//                int position = currentItemsCount > 0 ? currentItemsCount - 1 : 0;
//              mAdapter.notifyItemRemoved(position);
//              Log.e("onLoaded", "Current items count => " + currentItemsCount + "Position =>" + position);
//                mAdapter.notifyItemRangeInserted(position, newItemsCount);

                mAdapter.notifyDataSetChanged();
                if (mContainer.getVisibility() == View.GONE) {
                    setContainerShown(true, true);
                    if (mAdapter.getItemCount() == 0){
                        mEmtyView.setVisibility(View.VISIBLE);
                        if (Utilities.getInstance().isInternetOn()){
                            mEmtyView.setCompoundDrawablesWithIntrinsicBounds(null,
                                    ContextCompat.getDrawable(getActivity(), R.drawable.ic_mood_bad_black_48dp),
                                    null, null);
                            mEmtyView.setText(R.string.no_data_available);
                        } else {
                            mEmtyView.setCompoundDrawablesWithIntrinsicBounds(null,
                                    ContextCompat.getDrawable(getActivity(), R.drawable.ic_signal_wifi_off_black_48dp),
                                    null, null);
                            mEmtyView.setText(R.string.cannot_access_internet);
                        }
                    } else {
                        mEmtyView.setVisibility(View.GONE);
                    }
                }

                if(mSwipeRefreshLayout.isRefreshing())
                    mSwipeRefreshLayout.setRefreshing(false);
            }
        }, new PlaceAdapter.IViewHolderClicks() {
            @Override
            public void onItemClick(View view, Place object) {
                ArrayList<String> urlList = new ArrayList<>();
                for (Photo item:object.getPhotos()){
                    urlList.add(item.getPhotoFile().getUrl());
                }

                ParseProxyObject ppo = new ParseProxyObject(object);
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putStringArrayListExtra("photos", urlList);
                intent.putExtra("parseObject", ppo);
                startActivity(intent);
            }

            @Override
            public void onMenuItemClick(View view, Place object) {
                PopupMenu popupMenu = new PopupMenu(getActivity(), view);
                popupMenu.inflate(R.menu.menu_popup);
                popupMenu.setOnMenuItemClickListener(mAdapter);
                popupMenu.show();
                mAdapter.setClickedItem(object);
            }
        });

        recyclerView.setAdapter(mAdapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setContainerShown(boolean shown, boolean animate){
        if(shown){
            if(mContainer.getVisibility() == View.VISIBLE)
                return;
            if(animate)
            {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(MyApplication.getContext(), android.R.anim.fade_out));
                mContainer.startAnimation(AnimationUtils.loadAnimation(MyApplication.getContext(), android.R.anim.fade_in));
            } else {
                mProgressContainer.clearAnimation();
                mContainer.clearAnimation();
            }

            mProgressContainer.setVisibility(View.GONE);
            mContainer.setVisibility(View.VISIBLE);
        } else {
            if(animate)
            {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(MyApplication.getContext(), android.R.anim.fade_in));
                mContainer.startAnimation(AnimationUtils.loadAnimation(MyApplication.getContext(), android.R.anim.fade_out));
            } else {
                mProgressContainer.clearAnimation();
                mContainer.clearAnimation();
            }

            mProgressContainer.setVisibility(View.VISIBLE);
            mContainer.setVisibility(View.GONE);
        }
    }
}
