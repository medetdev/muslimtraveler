package com.medetzhakupov.muslimtraveler.fragments;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.medetzhakupov.muslimtraveler.MuslimTravelerConstants;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.Utilities;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class GalleryFragment extends Fragment {
    GridLayoutManager gridLayoutManager;

    public static GalleryFragment newInstance(Bundle args) {
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public GalleryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        gridLayoutManager = new GridLayoutManager(view.getContext(), getResources().getInteger(R.integer.number_of_gallery_rows));
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(new GalleryAdapter(getArguments().getStringArrayList(MuslimTravelerConstants.EXTRA_URLS)));
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_gallery, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_photo) {
            Utilities.getInstance().shareContent(getActivity(), "Test Data");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class GalleryAdapter extends RecyclerView.Adapter<ImageHolder>{

        private final DisplayImageOptions options;
        ArrayList<String> items;


        public GalleryAdapter(ArrayList<String> items){
            this.items = items;
            options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(android.R.color.darker_gray)
                    .displayer(new FadeInBitmapDisplayer(500))
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();
        }

        @Override
        public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_view, parent,false);
            ImageHolder holder = new ImageHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(ImageHolder holder, final int position) {
            ImageLoader.getInstance().displayImage(items.get(position), holder.imageView, options);
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList(MuslimTravelerConstants.EXTRA_URLS, items);
                    bundle.putInt(MuslimTravelerConstants.POSITION, position);
                    fm.beginTransaction()
                            .replace(R.id.container, FullImageFragment.newInstance(bundle))
                            .addToBackStack(FullImageFragment.class.getName())
                            .commit();
                }
            });
        }

        @Override
        public int getItemCount() {
            return items != null ? items.size() : 0;
        }
    }

    public static class ImageHolder extends RecyclerView.ViewHolder{
        public View view;
        public ImageView imageView;

        public ImageHolder(View v){
            super(v);
            view = v;
            imageView = (ImageView)v.findViewById(R.id.image_view);
        }
    }
}
