package com.medetzhakupov.muslimtraveler.fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.model.Feedback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FeedbackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeedbackFragment extends Fragment {
    public final static String FRAGMENT_TAG = "FeedbackFragment";

    EditText mFeedbackText;
    CheckBox mCheckBoxScreenshotLogs;
    ImageView mScreenshot;

    public static FeedbackFragment newInstance() {
        FeedbackFragment fragment = new FeedbackFragment();
        return fragment;
    }

    public FeedbackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        mFeedbackText = (EditText)view.findViewById(R.id.feedback_text);
        mCheckBoxScreenshotLogs = (CheckBox)view.findViewById(R.id.checkbox_screenshot_logs);
        mScreenshot = (ImageView)view.findViewById(R.id.screenshot);

        mFeedbackText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (count > 3)
                    getActivity().invalidateOptionsMenu();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_feedback, menu);
        super.onCreateOptionsMenu(menu, inflater);
        if (mFeedbackText.getText().toString().isEmpty())
            menu.findItem(R.id.action_send).setEnabled(false);
        else
            menu.findItem(R.id.action_send).setEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_send){
            sendFeedback();
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendFeedback()
    {
        Feedback feedback = new Feedback();
        feedback.setFeedback(mFeedbackText.getText().toString());
        feedback.setLogs("");
//            feedback.setScreenshotFile(new ParseFile());
        feedback.setSystemInformation("");
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.please_wait_sending_feedback));
        progressDialog.show();
        feedback.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                String message = getString(R.string.feedback_could_not_send);
                int resIcon = R.drawable.ic_error_black_24dp;
                if (e == null){
                    message = getString(R.string.feedback_has_been_sent);
                    resIcon = R.drawable.ic_assignment_turned_in_black_24dp;
                } else if (e.getCode() == ParseException.CONNECTION_FAILED ||
                        e.getCode() == ParseException.TIMEOUT){
                    message = getString(R.string.we_discovered_internet_problem);
                    resIcon = R.drawable.ic_signal_wifi_off_black_48dp;
                }
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setIcon(resIcon);
                alertBuilder.setTitle(R.string.feedback);
                alertBuilder.setMessage(message);
                AlertDialog dialog = alertBuilder.create();
                dialog.show();
                getActivity().onBackPressed();
            }
        });
    }
}
