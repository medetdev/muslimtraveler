package com.medetzhakupov.muslimtraveler.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.medetzhakupov.muslimtraveler.MuslimTravelerConstants;
import com.medetzhakupov.muslimtraveler.MyApplication;
import com.medetzhakupov.muslimtraveler.OnFragmentInteractionListener;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.Utilities;
import com.medetzhakupov.muslimtraveler.activities.DetailActivity;
import com.medetzhakupov.muslimtraveler.components.ParseProxyObject;
import com.medetzhakupov.muslimtraveler.model.Bookmark;
import com.medetzhakupov.muslimtraveler.model.Photo;
import com.medetzhakupov.muslimtraveler.model.Place;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.ui.ParseLoginBuilder;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SavedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SavedFragment extends Fragment {
    public static final String FRAGMENT_TAG = "SavedFragment";

    private ListView mListView;
    private View mContainer;
    private View mProgressContainer;
    private TextView mEmtyView;
    private Button mBtnSignIn;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PlaceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SavedFragment newInstance() {
        SavedFragment fragment = new SavedFragment();
        return fragment;
    }

    public SavedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_saved, container, false);
        mListView = (ListView) view.findViewById(R.id.list_view);
        mProgressContainer = view.findViewById(R.id.progress_container);
        mContainer = view.findViewById(R.id.container);
        mEmtyView = (TextView)view.findViewById(R.id.empty_view);
        mBtnSignIn = (Button)view.findViewById(R.id.btn_sing_in);
        if (MyApplication.getSharePreferences().getBoolean(MuslimTravelerConstants.LOGGED_IN, false)){
            setContainerShown(false, false);
            initListView(view);
        } else {
            setContainerShown(true, true);
            mBtnSignIn.setVisibility(View.VISIBLE);
            mBtnSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ParseLoginBuilder builder = new ParseLoginBuilder(getActivity());
                    startActivityForResult(builder.build(), MuslimTravelerConstants.LOGIN_ACTIVITY_CODE);
                }
            });
        }

        return view;
    }

    private void initListView(View view)
    {
        ParseQueryAdapter.QueryFactory<Place> factory =
                new ParseQueryAdapter.QueryFactory<Place>() {
                    public ParseQuery create() {
                        ParseUser user = ParseUser.getCurrentUser();
                        ParseRelation<Place> relation = user.getRelation("bookmarks");
                        ParseQuery<Place> query = relation.getQuery();
                        query.include("photos");
                        return query;
                    }
                };

        final CustomAdapter adapter = new CustomAdapter(view.getContext(), factory);

        adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<Place>() {
            public void onLoading() {
                // Trigger any "loading" UI
            }

            @Override
            public void onLoaded(List<Place> list, Exception e) {
                setContainerShown(true,true);
                if (e == null){
                    if (list.size() == 0){
                        mEmtyView.setVisibility(View.VISIBLE);
                        if (Utilities.getInstance().isInternetOn()){
                            mEmtyView.setCompoundDrawablesWithIntrinsicBounds(null,
                                    ContextCompat.getDrawable(getActivity(), R.drawable.ic_mood_bad_black_48dp),
                                    null, null);
                            mEmtyView.setText(R.string.no_data_available);
                        } else {
                            mEmtyView.setCompoundDrawablesWithIntrinsicBounds(null,
                                    ContextCompat.getDrawable(getActivity(), R.drawable.ic_signal_wifi_off_black_48dp),
                                    null, null);
                            mEmtyView.setText(R.string.cannot_access_internet);
                        }
                    } else {
                        mEmtyView.setVisibility(View.GONE);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });

        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Place object = adapter.getItem(i);
                ArrayList<String> urlList = new ArrayList<>();
                for (Photo item:object.getPhotos()){
                    urlList.add(item.getPhotoFile().getUrl());
                }

                ParseProxyObject ppo = new ParseProxyObject(object);
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putStringArrayListExtra("photos", urlList);
                intent.putExtra("parseObject", ppo);
                startActivity(intent);
            }
        });
    }

    public void setContainerShown(boolean shown, boolean animate){
        if(shown){
            if(mContainer.getVisibility() == View.VISIBLE)
                return;
            if(animate)
            {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(MyApplication.getContext(), android.R.anim.fade_out));
                mContainer.startAnimation(AnimationUtils.loadAnimation(MyApplication.getContext(), android.R.anim.fade_in));
            } else {
                mProgressContainer.clearAnimation();
                mContainer.clearAnimation();
            }

            mProgressContainer.setVisibility(View.GONE);
            mContainer.setVisibility(View.VISIBLE);
        } else {
            if(animate)
            {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(MyApplication.getContext(), android.R.anim.fade_in));
                mContainer.startAnimation(AnimationUtils.loadAnimation(MyApplication.getContext(), android.R.anim.fade_out));
            } else {
                mProgressContainer.clearAnimation();
                mContainer.clearAnimation();
            }

            mProgressContainer.setVisibility(View.VISIBLE);
            mContainer.setVisibility(View.GONE);
        }
    }

    class CustomAdapter extends ParseQueryAdapter<Place>
    {

        public CustomAdapter(Context context, QueryFactory<Place> queryFactory) {
            super(context, queryFactory);
        }

        @Override
        public int getCount() {
            return super.getCount();
        }



        @Override
        public View getItemView(Place place, View v, ViewGroup parent) {
            try{
                final ViewHolder holder;
                if (v == null) {
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.raw, parent, false);
                    holder = new ViewHolder();
                    holder.imageView = (ImageView)v.findViewById(R.id.imgAvatar);
                    holder.textViewTitle = (TextView)v.findViewById(R.id.title);
                    holder.textViewAddress = (TextView)v.findViewById(R.id.address);
                    holder.btnMenu = (ImageView)v.findViewById(R.id.btn_menu);
                    holder.btnMenu.setVisibility(View.GONE);
//                    holder.btnMenu.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
////                            mListener.onMenuItemClick(view, (Place)holder.btnMenu.getTag());
//                        }
//                    });
                    v.setTag(holder);
                } else {
                    holder = (ViewHolder)v.getTag();
                }

                holder.btnMenu.setTag(place);
                holder.textViewTitle.setText(place.getTitle());
                holder.textViewAddress.setText(place.getAddress());
                ArrayList<Photo> photos = place.getPhotos();
                if (photos != null && photos.size() > 0){
                    Photo photo = photos.get(0);
                    try {
                        ImageLoader.getInstance().displayImage(((Photo)photo.fetchIfNeeded()).getPhotoFile().getUrl(), holder.imageView, MyApplication.options, new SimpleImageLoadingListener(){
                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                super.onLoadingComplete(imageUri, view, loadedImage);
                            }

                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                super.onLoadingStarted(imageUri, view);
                            }
                        });
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }catch(Exception ex){
                Log.e("ParseAdapter", ex.toString());
            }
            return v;
        }

    }

    static class ViewHolder
    {
        ImageView imageView;
        TextView textViewTitle;
        TextView textViewAddress;
        ImageView btnMenu;
    }
}
