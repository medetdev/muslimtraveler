package com.medetzhakupov.muslimtraveler.components;

import android.content.Context;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

/**
 * Created by medetzhakupov on 7/22/15.
 */
public class CustomClusterManager<T extends ClusterItem> extends ClusterManager<T> {
    ICameraChangeListener cameraChangeListener;

    public CustomClusterManager(Context context, GoogleMap map, ICameraChangeListener listener) {
        super(context, map);
        cameraChangeListener = listener;
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        super.onCameraChange(cameraPosition);
        try{
            if (cameraChangeListener != null)
                cameraChangeListener.onCameraChange(cameraPosition);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public interface ICameraChangeListener
    {
        void onCameraChange(CameraPosition cameraPosition);
    }
}
