package com.medetzhakupov.muslimtraveler.components;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.model.Place;

/**
 * Created by medetzhakupov on 7/24/15.
 */
public class PlaceRenderer extends DefaultClusterRenderer<Place> {

    public PlaceRenderer(Context context, GoogleMap map, ClusterManager<Place> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(Place place, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(place, markerOptions);

        int resourceId = R.drawable.star;
        if (place.getCategory().getName().equals("Prayer")) {
            resourceId = R.drawable.mosquee;
        } else if (place.getCategory().getName().equals("Other")) {
            resourceId = R.drawable.star;
        } else if (place.getCategory().getName().equals("Outlet")) {
            resourceId = R.drawable.mall;
        } else if (place.getCategory().getName().equals("Food")) {
            resourceId = R.drawable.restaurant;
        }

        markerOptions.title(place.getTitle())
                .snippet(place.getAddress())
                .icon(BitmapDescriptorFactory
                        .fromResource(resourceId));
    }
}
