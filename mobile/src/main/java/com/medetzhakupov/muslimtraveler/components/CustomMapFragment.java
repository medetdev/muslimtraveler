package com.medetzhakupov.muslimtraveler.components;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by medetzhakupov on 7/22/15.
 */
public class CustomMapFragment extends SupportMapFragment {

    private View mOriginalContentView;
    private TouchableWrapper mTouchView;
    private ITouchListener touchListener;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, container,
                savedInstanceState);

        mTouchView = new TouchableWrapper(getActivity());
        mTouchView.addView(mOriginalContentView);

        return mTouchView;
    }

    @Nullable
    @Override
    public View getView() {
        return mTouchView;
    }

    public void setTouchListener(ITouchListener listener)
    {
        touchListener = listener;
    }

    public class TouchableWrapper extends FrameLayout {

        public TouchableWrapper(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
            if (touchListener != null)
                touchListener.onTouch(ev);

            return super.dispatchTouchEvent(ev);
        }

    }

    public interface ITouchListener
    {
        void onTouch(MotionEvent event);
    }
}
