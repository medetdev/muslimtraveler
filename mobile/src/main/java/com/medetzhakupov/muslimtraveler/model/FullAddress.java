package com.medetzhakupov.muslimtraveler.model;

import java.io.Serializable;

/**
 * Created by mac on 4/17/15.
 */
public class FullAddress implements Serializable{
    private double latitude;

    private double longitude;

    private String address;

    private String locality;

    private String city;

    private String country;

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddress() {
        return address;
    }

    public String getLocality() {
        return locality;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(address).append("\n");
        result.append(locality).append("\n");
        result.append(country);

        return result.toString();
    }
}
