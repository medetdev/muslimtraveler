package com.medetzhakupov.muslimtraveler.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;

/**
 * Created by medetzhakupov on 6/15/15.
 */
@ParseClassName("Bookmark")
public class Bookmark extends ParseObject{
    public ParseUser getUser(){
        return (ParseUser) get("createdBy");
    }

    public void setUser(ParseUser user){
        put("createdBy", user);
    }

    public static ParseQuery<Bookmark> getQuery() {
        return ParseQuery.getQuery(Bookmark.class);
    }
}

