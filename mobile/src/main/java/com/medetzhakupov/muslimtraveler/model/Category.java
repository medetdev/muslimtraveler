package com.medetzhakupov.muslimtraveler.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Medet Zhakupov on 7/4/2015.
 */
@ParseClassName("Category")
public class Category extends ParseObject {
    public String getName(){
        return getString("name");
    }

    public void setName(String value){
        put("name", value);
    }

    public static ParseQuery<Category> getQuery() {
        return ParseQuery.getQuery(Category.class);
    }
}
