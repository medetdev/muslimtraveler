package com.medetzhakupov.muslimtraveler.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Medet Zhakupov on 7/4/2015.
 */
@ParseClassName("Photo")
public class Photo extends ParseObject {
    private ParseFile parseFile;

    public ParseFile getPhotoFile(){
        parseFile = getParseFile("photo");
        return parseFile;
    }

    public void setPhotoFile(ParseFile value){
        parseFile = value;
        put("photo", parseFile);
    }

    public static ParseQuery<Photo> getQuery() {
        return ParseQuery.getQuery(Photo.class);
    }
}