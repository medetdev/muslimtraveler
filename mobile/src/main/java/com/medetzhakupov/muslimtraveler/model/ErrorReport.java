package com.medetzhakupov.muslimtraveler.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by medetzhakupov on 7/14/15.
 */
@ParseClassName("ErrorReport")
public class ErrorReport extends ParseObject {

    public void setTitle(String title){
        put("title", title);
    }

    public String getTitle()
    {
        return getString("title");
    }

    public void setMessage(String message){
        put("message", message);
    }

    public String getMessage()
    {
        return getString("message");
    }

    public static ParseQuery<ErrorReport> getQuery() {
        return ParseQuery.getQuery(ErrorReport.class);
    }
}
