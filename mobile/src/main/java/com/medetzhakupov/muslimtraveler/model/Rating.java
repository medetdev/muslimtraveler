package com.medetzhakupov.muslimtraveler.model;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by Medet Zhakupov on 7/4/2015.
 */
@ParseClassName("Rating")
public class Rating extends ParseObject {

    public int getRate(){
        return getInt("rate");
    }

    public void setRate(int value){
        put("rate", value);
    }

    public String getComment(){
        return getString("comment");
    }

    public void setComment(String value){
        put("comment", value);
    }

    public Place getPlace(){
        return (Place) get("place");
    }

    public void setPlace(Place value){
        put("place", value);
    }

    public ParseUser getUser(){
        return (ParseUser) get("createdBy");
    }

    public void setUser(ParseUser user){
        put("createdBy", user);
    }

    public static ParseQuery<Rating> getQuery() {
        return ParseQuery.getQuery(Rating.class);
    }
}
