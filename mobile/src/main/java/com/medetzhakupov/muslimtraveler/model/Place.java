package com.medetzhakupov.muslimtraveler.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Medet Zhakupov on 7/4/2015.
 */

@ParseClassName("Place")
public class Place extends ParseObject implements Serializable, ClusterItem{

    public boolean getIsPublished(){
        return getBoolean("isPublished");
    }

    public void setIsPublished(boolean isPublished){
        put("isPublished", isPublished);
    }

    public ParseGeoPoint getLocation() {
        return getParseGeoPoint("location");
    }

    public void setLocation(ParseGeoPoint value){
        put("location", value);
    }

    public String getAddress() {
        return getString("address");
    }

    public void setAddress(String value){
        put("address", value);
    }

    public String getCity() {
        return getString("city");
    }

    public void setCity(String value){
        put("city", value);
    }

    public String getCountry() {
        return getString("country");
    }

    public void setCountry(String value){
        put("country", value);
    }

    public String getTitle(){
        return getString("title");
    }

    public void setTitle(String value){
        put("title", value);
    }

    public String getPhone(){
        return getString("phone");
    }

    public void setPhone(String value){
        put("phone", value);
    }

    public String getDescription(){
        return getString("description");
    }

    public void setDescription(String value){
        put("description", value);
    }

    public Category getCategory(){
        return (Category) get("category");
    }

    public void setCategory(Category category){
        put("category", category);
    }

    public void setMapSnapshot(ParseFile parseFile)
    {
        put("mapSnapshot", parseFile);
    }

    public ParseFile getMapSnapshot()
    {
        return getParseFile("mapSnapshot");
    }

    public ArrayList<Photo> getPhotos(){
        return (ArrayList<Photo>) get("photos");
    }

    public void setPhotos(ArrayList<Photo> value){
        put("photos", value);
    }

    public static ParseQuery<Place> getQuery() {
        return ParseQuery.getQuery(Place.class);
    }

    @Override
    public LatLng getPosition() {
        ParseGeoPoint location = getLocation();
        return new LatLng(location.getLatitude(), location.getLongitude());
    }
}
