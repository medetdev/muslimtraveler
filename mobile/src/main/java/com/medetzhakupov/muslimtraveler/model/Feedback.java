package com.medetzhakupov.muslimtraveler.model;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

/**
 * Created by medetzhakupov on 7/30/15.
 */
@ParseClassName("Feedback")
public class Feedback extends ParseObject {

    public void setFeedback(String feedback){
        put("feedback", feedback);
    }

    public void setSystemInformation(String systemInformation){
        put("systemInformation", systemInformation);
    }

    public void setLogs(String logs){
        put("logs", logs);
    }

    public void setScreenshotFile(ParseFile value){
        put("screenshot", value);
    }
}
