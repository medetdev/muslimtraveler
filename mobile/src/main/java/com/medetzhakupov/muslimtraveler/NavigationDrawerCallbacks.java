package com.medetzhakupov.muslimtraveler;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
