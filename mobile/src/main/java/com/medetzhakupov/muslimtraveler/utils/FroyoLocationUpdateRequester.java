package com.medetzhakupov.muslimtraveler.utils;

/**
 * Created by medetzhakupov on 7/24/15.
 */

import android.app.PendingIntent;
import android.location.LocationManager;

import com.medetzhakupov.muslimtraveler.MuslimTravelerConstants;

/**
 * Provides support for initiating active and passive location updates
 * optimized for the Froyo release. Includes use of the Passive Location Provider.
 *
 * Uses broadcast Intents to notify the app of location changes.
 */
public class FroyoLocationUpdateRequester extends LocationUpdateRequester{

    public FroyoLocationUpdateRequester(LocationManager locationManager) {
        super(locationManager);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void requestPassiveLocationUpdates(long minTime, long minDistance, PendingIntent pendingIntent) {
        // Froyo introduced the Passive Location Provider, which receives updates whenever a 3rd party app
        // receives location updates.
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MuslimTravelerConstants.MAX_TIME, MuslimTravelerConstants.MAX_DISTANCE, pendingIntent);
    }
}
