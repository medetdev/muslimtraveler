package com.medetzhakupov.muslimtraveler.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.model.Category;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mac on 4/16/15.
 */
public class CategoryAdapter extends ArrayAdapter<Category> {

    // declaring our ArrayList of categories
    private ArrayList<Category> objects;

    public CategoryAdapter(Context context, int resource, List<Category> objects) {
        super(context, resource, objects);
        this.objects = new ArrayList<>(objects);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;


        if (view == null){
            view = super.getView(position, convertView, parent);
        }

        Category item = objects.get(position);

        if (item != null){
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextColor(R.color.half_black);
            String name = item.getName();
            textView.setText(name);
        }

        return view;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = convertView;


        if (view == null){
            view = super.getDropDownView(position, convertView, parent);
        }

        Category item = objects.get(position);

        if (item != null){
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextColor(R.color.half_black);
            String name = item.getName();
            textView.setText(name);
        }

        return view;
    }

    @Override
    public Category getItem(int position) {
        return objects.get(position);
    }

    public int getSelectedItemPosition(String name){
        int position = 0;
        if (objects != null){
            for (int i = 0; i < objects.size(); i++){
                if (objects.get(i).getName().equals(name))
                    position = i;
            }

        }
        return position;
    }
}
