package com.medetzhakupov.muslimtraveler.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.medetzhakupov.muslimtraveler.MyApplication;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.Utilities;
import com.medetzhakupov.muslimtraveler.model.Bookmark;
import com.medetzhakupov.muslimtraveler.model.Photo;
import com.medetzhakupov.muslimtraveler.model.Place;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
* Created by Medet Zhakupov on 23/4/2015.
*/
public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.VHolder> implements PopupMenu.OnMenuItemClickListener{
    private ParseQueryAdapter<Place> parseAdapter;
    private IViewHolderClicks mListener;
    private ViewGroup parseParent;
    private PlaceAdapter placeAdapter = this;
    private ILoader mLoader;
    private Place clickedItem;
    private Context context;


    public PlaceAdapter(Context context, ViewGroup parentIn, ILoader loader, IViewHolderClicks listener) {
        this.context = context;
        parseParent = parentIn;
        mLoader = loader;
        mListener = listener;
        ParseQueryAdapter.QueryFactory<Place> factory =
                new ParseQueryAdapter.QueryFactory<Place>() {
                    public ParseQuery create() {
                        ParseQuery query = new ParseQuery("Place");
                        query.whereEqualTo("isPublished", true);
                        query.include("photos");
                        return query;
                    }
                };

        parseAdapter = new ParseQueryAdapter<Place>(context, factory) {
            @Override
            public View getItemView(final Place place, View v, ViewGroup parent) {
                try{
                    final ViewHolder holder;
                    if (v.getTag() == null) {
                        holder = new ViewHolder();
                        holder.imageView = (ImageView)v.findViewById(R.id.imgAvatar);
                        holder.textViewTitle = (TextView)v.findViewById(R.id.title);
                        holder.textViewAddress = (TextView)v.findViewById(R.id.address);
                        holder.btnMenu = (ImageView)v.findViewById(R.id.btn_menu);
                        holder.btnMenu.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mListener.onMenuItemClick(view, (Place)holder.btnMenu.getTag());
                            }
                        });
                        v.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mListener.onItemClick(view, (Place)holder.btnMenu.getTag());
                            }
                        });
                        v.setTag(holder);
                    } else {
                        holder = (ViewHolder)v.getTag();
                    }

                    holder.btnMenu.setTag(place);
                    holder.textViewTitle.setText(place.getTitle());
                    holder.textViewAddress.setText(place.getAddress());
                    ArrayList<Photo> photos = place.getPhotos();
                    if (photos != null && photos.size() > 0){
                        Photo photo = photos.get(0);
                        try {
                            ImageLoader.getInstance().displayImage(((Photo)photo.fetchIfNeeded()).getPhotoFile().getUrl(), holder.imageView, MyApplication.options, new SimpleImageLoadingListener(){
                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    super.onLoadingComplete(imageUri, view, loadedImage);
                                }

                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                    super.onLoadingStarted(imageUri, view);
                                }
                            });
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }catch(Exception ex){
                    Log.e("ParseAdapter", ex.toString());
                }

                return v;
            }
        };
        parseAdapter.setObjectsPerPage(10);
        parseAdapter.addOnQueryLoadListener(new OnQueryLoadListener());
        parseAdapter.loadObjects();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.action_share:
                Utilities.getInstance().shareContent(context, clickedItem.getDescription());
                break;
            case R.id.action_bookmark:
                Utilities.getInstance().saveBookmark(clickedItem.getObjectId(), new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null)
                            Utilities.getInstance().showToast(context, context.getString(R.string.saved_to_bookmark));
                    }
                });
                break;
        }
        return false;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PlaceAdapter.VHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);
        PlaceAdapter.VHolder vh = new PlaceAdapter.VHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PlaceAdapter.VHolder holder, int position) {
        try{
            if(parseAdapter.getItemViewType(position) == 1){
                holder.mView.findViewById(R.id.raw_root).setVisibility(View.GONE);
                holder.mView.findViewById(R.id.progress_container).setVisibility(View.VISIBLE);
//                parseAdapter.getView(position, holder.mView, parseParent);
                parseAdapter.loadNextPage();
            } else{
                holder.mView.findViewById(R.id.progress_container).setVisibility(View.GONE);
                holder.mView.findViewById(R.id.raw_root).setVisibility(View.VISIBLE);
                parseAdapter.getView(position, holder.mView, parseParent);
            }
        }catch (Exception ex){Log.e("ParseAdapter", ex.toString());}
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return parseAdapter.getCount();
    }

    public void setClickedItem(Place clickedItem) {
        this.clickedItem = clickedItem;
    }

    public static class VHolder extends RecyclerView.ViewHolder {
        public View mView;

        public VHolder(View v) {
            super(v);
            mView = v;
        }
    }

    static class ViewHolder
    {
        ImageView imageView;
        TextView textViewTitle;
        TextView textViewAddress;
        ImageView btnMenu;
    }

    public interface IViewHolderClicks {
        void onItemClick(View view, Place object);
        void onMenuItemClick(View view, Place object);
    }

    public class OnQueryLoadListener implements ParseQueryAdapter.OnQueryLoadListener<Place> {

        public void onLoading() {

        }

        public void onLoaded(List<Place> objects, Exception e) {
            if (e == null){
                mLoader.onLoadFinish(parseAdapter.getCount(), objects.size());
            } else if(((ParseException)e).getCode() == ParseException.CONNECTION_FAILED){
                mLoader.onLoadFinish(0, 0);
            }
        }
    }

    public interface ILoader {
        void onLoadFinish(int currentItemsCount, int newItemsCount);
    }
}


