package com.medetzhakupov.muslimtraveler.adapters;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.medetzhakupov.muslimtraveler.MyApplication;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.activities.DetailActivity;
import com.medetzhakupov.muslimtraveler.model.Photo;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.ParseException;

import java.util.ArrayList;

/**
 * Created by Medet Zhakupov on 28/4/2015.
 */
public class DetailPagerAdapter extends PagerAdapter {

    ArrayList<String> photos;

    public DetailPagerAdapter(ArrayList<String> photos)
    {
        this.photos = photos;
    }

    @Override
    public int getCount() {
        if (photos == null)
            return 0;
        return photos.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.pager_raw, null);
        ImageView imageView = (ImageView)view.findViewById(R.id.image);
        try {
            ImageLoader.getInstance().displayImage(photos.get(position),
                    imageView,
                    MyApplication.options);
        } catch (Exception e) {
            e.printStackTrace();
        }
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
