package com.medetzhakupov.muslimtraveler.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.medetzhakupov.muslimtraveler.MyApplication;
import com.medetzhakupov.muslimtraveler.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Medet Zhakupov on 16/4/2015.
 */
public class HorizontalRecyclerAdapter extends RecyclerView.Adapter<HorizontalRecyclerAdapter.Holder> implements View.OnClickListener {

    Context mContext;
    IAdapterItemClickListener mListener;
    ArrayList<String> mListPhotos;

    public HorizontalRecyclerAdapter(Context ctx, ArrayList<String> listPhotos, IAdapterItemClickListener listener){
        mContext = ctx;
        mListPhotos = listPhotos;
        mListener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.image_layout, parent, false);
        Holder holder = new Holder(view);
        holder.mImageView.setOnClickListener(this);
        holder.mDeleteIcon.setOnClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        holder.mImageView.setTag(position);
        holder.mDeleteIcon.setTag(position);
        if (mListPhotos.size() > position){
            ImageLoader.getInstance()
                    .displayImage("file:///"+mListPhotos.get(position),
                            holder.mImageView,
                            MyApplication.options);
            holder.mDeleteIcon.setVisibility(View.VISIBLE);
        } else {
            holder.mImageView.setImageResource(R.drawable.ic_add_to_photos_grey600_48dp);
            holder.mDeleteIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (mListPhotos == null){
            mListPhotos = new ArrayList<>();
        }
        if (mListPhotos.size() == 0 ){
            count = 2;
        } else {
            count = mListPhotos.size() + 1;
        }
        return count;
    }

    @Override
    public void onClick(View view) {
        if (mListener != null){
            mListener.onItemClick(view.getId (), (int)view.getTag());
        }
    }

    public ArrayList<String> getData(){
        return mListPhotos;
    }

    public void setData(ArrayList list){
        mListPhotos.addAll(list);
        this.notifyDataSetChanged();
    }

    public void setData(String photoFile){
        mListPhotos.add(photoFile);
        this.notifyDataSetChanged();
    }

    public void deleteData(int position){
        if (position < mListPhotos.size()){
            mListPhotos.remove(position);
            this.notifyItemRemoved(position);
            this.notifyDataSetChanged();
        }
    }

    public void reset() {
        mListPhotos.clear();
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder
    {
        public ImageView mImageView;
        public ImageView mDeleteIcon;

        public Holder(View view){
            super(view);
            mImageView = (ImageView)view.findViewById(R.id.image_view);
            mDeleteIcon= (ImageView)view.findViewById(R.id.delete_icon);
        }
    }

    public interface IAdapterItemClickListener{
        public void onItemClick(int viewId, int position);
    }
}

