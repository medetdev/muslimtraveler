package com.medetzhakupov.muslimtraveler.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.medetzhakupov.muslimtraveler.MuslimTravelerConstants;
import com.medetzhakupov.muslimtraveler.MyApplication;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.Utilities;
import com.medetzhakupov.muslimtraveler.components.ParseProxyObject;
import com.medetzhakupov.muslimtraveler.model.ErrorReport;
import com.medetzhakupov.muslimtraveler.model.Place;
import com.nineoldandroids.view.ViewHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import at.markushi.ui.CircleButton;

public class DetailActivity extends ActionBarActivity implements ObservableScrollViewCallbacks, View.OnClickListener {

private static final float MAX_TEXT_SCALE_DELTA = 0.3f;
    private static final boolean TOOLBAR_IS_STICKY = true;

    private Toolbar mToolbar;
    private ImageView mImageView;
    private Dialog dialogReportError;
    private EditText etPopupTitle, etPopupMessage;
    private CircleButton btnPopupSubmit;
    private View mOverlayView, mMenuContainer;
    private ObservableScrollView mScrollView;
    private TextView mTitleTextView, mAddressTextView, mCityTextView,  mDesTextView;
    private ImageButton mBtnBookmark, mBtnComment, mBtnGallery, mBtnError;
//    private View mFab;
    private int mActionBarSize;
    private int mFlexibleSpaceShowFabOffset;
    private int mFlexibleSpaceImageHeight;
    private int mFabMargin;
    private int mToolbarColor;
    private boolean mFabIsShown;
    private ParseProxyObject ppo;
    private ArrayList<String> urlList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Intent intent = getIntent();
        ppo = (ParseProxyObject) intent.getSerializableExtra("parseObject");
        urlList = intent.getStringArrayListExtra("photos");

        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height);
        mFlexibleSpaceShowFabOffset = getResources().getDimensionPixelSize(R.dimen.flexible_space_show_fab_offset);
        mActionBarSize = getActionBarSize();
        mToolbarColor = getResources().getColor(R.color.myPrimaryColor);

        if (!TOOLBAR_IS_STICKY) {
            mToolbar.setBackgroundColor(Color.TRANSPARENT);
        }
        mImageView = (ImageView)findViewById(R.id.image_view);
        ImageLoader.getInstance().displayImage(urlList.get(0), mImageView, MyApplication.options);
        mBtnBookmark = (ImageButton)findViewById(R.id.bookmark_button);
        mBtnComment = (ImageButton)findViewById(R.id.comment_button);
        mBtnGallery = (ImageButton)findViewById(R.id.gallery_button);
        mBtnError = (ImageButton)findViewById(R.id.report_error_button);
        mOverlayView = findViewById(R.id.overlay);
        mMenuContainer = findViewById(R.id.menu_container);
        mScrollView = (ObservableScrollView) findViewById(R.id.scroll);
        mScrollView.setScrollViewCallbacks(this);
        mTitleTextView = (TextView) findViewById(R.id.title);
        mAddressTextView = (TextView) findViewById(R.id.address);
        mDesTextView = (TextView) findViewById(R.id.description);
        mCityTextView = (TextView) findViewById(R.id.city);

        mTitleTextView.setText(ppo.getString("title"));
        mAddressTextView.setText(ppo.getString("address"));
        mCityTextView.setText(ppo.getString("city"));

        mBtnBookmark.setOnClickListener(this);
        mBtnGallery.setOnClickListener(this);
        mBtnComment.setOnClickListener(this);
        mBtnError.setOnClickListener(this);
        setUpReportErrorPopup();

//        mDesTextView.setText(ppo.getString("description"));

        //String location = ppo.getString("location");

        setTitle(null);

        ScrollUtils.addOnGlobalLayoutListener(mScrollView, new Runnable() {
            @Override
            public void run() {
                //mScrollView.scrollTo(0, mFlexibleSpaceImageHeight - mActionBarSize);

                // If you'd like to start from scrollY == 0, don't write like this:
                // mScrollView.scrollTo(0, 0);
                // The initial scrollY is 0, so it won't invoke onScrollChanged().
                // To do this, use the following:
                onScrollChanged(0, false, false);

                // You can also achieve it with the following codes.
                // This causes scroll change from 1 to 0.
                //mScrollView.scrollTo(0, 1);
                //mScrollView.scrollTo(0, 0);
            }
        });

        if (ParseUser.getCurrentUser() != null){
            ParseUser user = ParseUser.getCurrentUser();
            ParseRelation<Place> relation = user.getRelation("bookmraks");
            relation.getQuery().whereEqualTo("objectId", ppo.getString("objectId")).findInBackground(new FindCallback<Place>() {
                @Override
                public void done(List<Place> list, ParseException e) {
                    if (e == null && list != null && list.size() > 0) {
                        mBtnBookmark.setImageResource(R.drawable.ic_bookmark_check_grey600_24dp);
                    } else {
                        mBtnBookmark.setImageResource(R.drawable.ic_bookmark_grey600_24dp);
                    }
                }
            });
        }
    }

        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            Utilities.getInstance().shareContent(this, "Test Data");
            return true;
        } else if (id == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if(dialogReportError != null && dialogReportError.isShowing()){
            dialogReportError.dismiss();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        // Translate overlay and image
        float flexibleRange = mFlexibleSpaceImageHeight - mActionBarSize;
        int minOverlayTransitionY = mActionBarSize - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-scrollY / 2, minOverlayTransitionY, 0));

        // Change alpha of overlay
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) scrollY / flexibleRange, 0, 1));

        // Scale title text
        float scale = 1 + ScrollUtils.getFloat((flexibleRange - scrollY) / flexibleRange, 0, MAX_TEXT_SCALE_DELTA);
        ViewHelper.setPivotX(mTitleTextView, 0);
        ViewHelper.setPivotY(mTitleTextView, 0);
        ViewHelper.setScaleX(mTitleTextView, scale);
        ViewHelper.setScaleY(mTitleTextView, scale);

        // Translate title text
        int maxTitleTranslationY = (int) (mFlexibleSpaceImageHeight - mTitleTextView.getHeight() * scale);
        int titleTranslationY = maxTitleTranslationY - scrollY;
        if (TOOLBAR_IS_STICKY) {
            titleTranslationY = Math.max(0, titleTranslationY);
        }
        ViewHelper.setTranslationY(mTitleTextView, titleTranslationY);
        ViewHelper.setTranslationY(mMenuContainer, titleTranslationY);

        if (TOOLBAR_IS_STICKY) {
            // Change alpha of toolbar background
            if (-scrollY + mFlexibleSpaceImageHeight <= mActionBarSize) {
                mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(1, mToolbarColor));
            } else {
                mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, mToolbarColor));
            }
        } else {
            // Translate Toolbar
            if (scrollY < mFlexibleSpaceImageHeight) {
                ViewHelper.setTranslationY(mToolbar, 0);
            } else {
                ViewHelper.setTranslationY(mToolbar, -scrollY);
            }
        }
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

//    private void showFab() {
//        if (!mFabIsShown) {
//            ViewPropertyAnimator.animate(mFab).cancel();
//            ViewPropertyAnimator.animate(mFab).scaleX(1).scaleY(1).setDuration(200).start();
//            mFabIsShown = true;
//        }
//    }

//    private void hideFab() {
//        if (mFabIsShown) {
//            ViewPropertyAnimator.animate(mFab).cancel();
//            ViewPropertyAnimator.animate(mFab).scaleX(0).scaleY(0).setDuration(200).start();
//            mFabIsShown = false;
//        }
//    }

    private void setUpReportErrorPopup()
    {
        dialogReportError = new Dialog(this);
        dialogReportError.requestWindowFeature(getWindow().FEATURE_NO_TITLE);
        dialogReportError.setContentView(R.layout.popup_report_error_layout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogReportError.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        etPopupTitle = (EditText)dialogReportError.findViewById(R.id.edit_text_title);
        etPopupMessage = (EditText)dialogReportError.findViewById(R.id.edit_text_message);
        btnPopupSubmit = (CircleButton)dialogReportError.findViewById(R.id.btn_submit);
        btnPopupSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    final ParseUser user = ParseUser.getCurrentUser();
                    if (user == null){
                        Toast.makeText(view.getContext(),
                                R.string.only_signed_users_can_report_error,
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Utilities.getInstance().validateEditText(etPopupTitle) &&
                            Utilities.getInstance().validateEditText(etPopupMessage)){
                        final ErrorReport errorReport = new ErrorReport();
                        errorReport.setTitle(etPopupTitle.getText().toString());
                        errorReport.setMessage(etPopupMessage.getText().toString());
                        errorReport.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                ParseRelation<ErrorReport> relation = user.getRelation("reports");
                                relation.add(errorReport);
                                user.saveInBackground();
                            }
                        });
                        dismissPopup();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void showPopup()
    {
        if(dialogReportError != null && !dialogReportError.isShowing()){
            dialogReportError.show();
        }
    }

    private void dismissPopup()
    {
        if(dialogReportError != null && dialogReportError.isShowing()){
            dialogReportError.dismiss();
        }
    }

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()){
            case R.id.bookmark_button:
                Utilities.getInstance().saveBookmark(ppo.getString("objectId"), new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null){
                            mBtnBookmark.setImageResource(R.drawable.ic_bookmark_check_grey600_24dp);
                            Utilities.getInstance().showToast(view.getContext(), view.getContext().getString(R.string.saved_to_bookmark));
                        }
                    }
                });
                break;
            case R.id.comment_button:
                
                break;
            case R.id.gallery_button:
                Intent intent = new Intent(this, GalleryActivity.class);
                intent.putExtra(MuslimTravelerConstants.EXTRA_URLS, urlList);
                intent.putExtra(MuslimTravelerConstants.TITLE, ppo.getString("title"));
                this.startActivity(intent);
                break;

            case R.id.report_error_button:
                showPopup();
                break;
        }
    }
}
