package com.medetzhakupov.muslimtraveler.activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.medetzhakupov.muslimtraveler.MuslimTravelerConstants;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.fragments.GalleryFragment;

public class GalleryActivity extends ActionBarActivity {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        setTitle(getIntent().getStringExtra(MuslimTravelerConstants.TITLE));
        FragmentManager fm = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(MuslimTravelerConstants.EXTRA_URLS, getIntent().getStringArrayListExtra(MuslimTravelerConstants.EXTRA_URLS));
        fm.beginTransaction()
                .replace(R.id.container, GalleryFragment.newInstance(bundle))
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
