package com.medetzhakupov.muslimtraveler.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.ProfilePictureView;
import com.google.android.gms.maps.model.VisibleRegion;
import com.medetzhakupov.muslimtraveler.LocationTracerService;
import com.medetzhakupov.muslimtraveler.MuslimTravelerConstants;
import com.medetzhakupov.muslimtraveler.MyApplication;
import com.medetzhakupov.muslimtraveler.NavigationDrawerCallbacks;
import com.medetzhakupov.muslimtraveler.Utilities;
import com.medetzhakupov.muslimtraveler.fragments.HelpAndFeedbackFragment;
import com.medetzhakupov.muslimtraveler.fragments.NavigationDrawerFragment;
import com.medetzhakupov.muslimtraveler.OnFragmentInteractionListener;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.fragments.SavedFragment;
import com.medetzhakupov.muslimtraveler.fragments.ViewPagerTabFragmentParentFragment;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.ui.ParseLoginBuilder;
import com.quinny898.library.persistentsearch.SearchBox;
import com.quinny898.library.persistentsearch.SearchResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerCallbacks, OnFragmentInteractionListener {

    private static final String TAG = "MainActivity";
    private SearchBox search;
    public Toolbar mToolbar;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        search = (SearchBox) findViewById(R.id.searchbox);
        search.setLogoText(getTitle().toString());
        search.enableVoiceRecognition(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            mToolbar.setElevation(0f);
        }
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        // populate the navigation drawer

        if(MyApplication.getSharePreferences().getBoolean(MuslimTravelerConstants.FIRST_TIME_STR, true)){
            ParseLoginBuilder builder = new ParseLoginBuilder(MainActivity.this);
            startActivityForResult(builder.build(), MuslimTravelerConstants.LOGIN_ACTIVITY_CODE);
        } else {
            login();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1234 && resultCode == RESULT_OK) {
            ArrayList<String> matches = new ArrayList<>();
                    matches.add(data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0));
            search.populateEditText(matches);
        }

        if (requestCode == MuslimTravelerConstants.LOGIN_ACTIVITY_CODE && resultCode == RESULT_OK){
            MyApplication.getSharePreferences().edit().putBoolean(MuslimTravelerConstants.FIRST_TIME_STR, false).commit();
            login();
        } else if (requestCode == MuslimTravelerConstants.LOGIN_ACTIVITY_CODE && resultCode == RESULT_CANCELED){
            MyApplication.getSharePreferences().edit().putBoolean(MuslimTravelerConstants.FIRST_TIME_STR, false).commit();
            MyApplication.getSharePreferences().edit().putBoolean(MuslimTravelerConstants.LOGGED_IN, false).apply();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void openSearch() {
        mToolbar.setTitle("");
        search.revealFromMenuItem(R.id.action_search, this);
//        for (int x = 0; x < 10; x++) {
//            SearchResult option = new SearchResult("Result "
//                    + Integer.toString(x),
//                    ContextCompat.getDrawable(this,
//                            R.drawable.ic_history_black_24dp));
//            search.addSearchable(option);
//        }
        search.setMenuListener(new SearchBox.MenuListener() {

            @Override
            public void onMenuClick() {
                // Hamburger has been clicked
                Toast.makeText(MainActivity.this, "Menu click",
                        Toast.LENGTH_LONG).show();
            }

        });
        search.setSearchListener(new SearchBox.SearchListener() {

            @Override
            public void onSearchOpened() {
                // Use this to tint the screen

            }

            @Override
            public void onSearchClosed() {
                // Use this to un-tint the screen
                closeSearch();
            }

            @Override
            public void onSearchTermChanged() {
                // React to the search term changing
                // Called after it has updated results
            }

            @Override
            public void onSearch(String searchTerm) {
                Toast.makeText(MainActivity.this, searchTerm + " Searched",
                        Toast.LENGTH_LONG).show();
                mToolbar.setTitle(searchTerm);

            }

            @Override
            public void onSearchCleared() {

            }

        });

    }

    protected void closeSearch() {
        search.hideCircularly(this);
        if(search.getSearchText().isEmpty())mToolbar.setTitle(getTitle());
    }


    private void login()
    {
        ParseUser user = ParseUser.getCurrentUser();
        if (user != null){
            SharedPreferences.Editor edit = MyApplication.getSharePreferences().edit();
            edit.putBoolean(MuslimTravelerConstants.LOGGED_IN, true);
            edit.apply();

            if (user.has("profile")) {
                try {
                    JSONObject userProfile = user.getJSONObject("profile");
                    if (userProfile != null){
                        mNavigationDrawerFragment.setUserData(userProfile.getString("name"), userProfile.getString("email"), BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
                    } else{
                        mNavigationDrawerFragment.setUserData((String) user.get("name"), "", BitmapFactory.decodeResource(getResources(), R.drawable.avatar));
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "Error parsing saved user data.");
                }
            }

            Toast.makeText(this, getString(R.string.signed_in_successfully), Toast.LENGTH_SHORT).show();
        } else {
            MyApplication.getSharePreferences().edit().putBoolean(MuslimTravelerConstants.LOGGED_IN, false).apply();
        }
        invalidateOptionsMenu();
    }

    private void logout()
    {
        ParseUser user = ParseUser.getCurrentUser();
        if (user != null)
            user.logOut();
        SharedPreferences.Editor edit = MyApplication.getSharePreferences().edit();
        edit.putBoolean(MuslimTravelerConstants.LOGGED_IN, false);
        edit.apply();
        mNavigationDrawerFragment.clearUserData();
        invalidateOptionsMenu();
        Toast.makeText(this, getString(R.string.sign_out_successfully), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        // update the main content by replacing fragments
        switch (position){
            case 0:
                if (fm.findFragmentByTag(ViewPagerTabFragmentParentFragment.FRAGMENT_TAG) == null) {
                    ft.replace(R.id.container, new ViewPagerTabFragmentParentFragment(),
                            ViewPagerTabFragmentParentFragment.FRAGMENT_TAG);
                }
                break;
            case 1:
                if (fm.findFragmentByTag(SavedFragment.FRAGMENT_TAG) == null) {
                    ft.replace(R.id.container, SavedFragment.newInstance(), SavedFragment.FRAGMENT_TAG);
                }
                break;
            case 2:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            case 3:
                break;
            case 4:
                if (fm.findFragmentByTag(HelpAndFeedbackFragment.FRAGMENT_TAG) == null) {
                    ft.replace(R.id.container, HelpAndFeedbackFragment.newInstance(), HelpAndFeedbackFragment.FRAGMENT_TAG);
                }
                break;
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
        fm.executePendingTransactions();
    }


    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else if(search.isShown())
            closeSearch();
        else
            super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            getMenuInflater().inflate(R.menu.main, menu);
            if (MyApplication.getSharePreferences().getBoolean(MuslimTravelerConstants.LOGGED_IN, false)) {
                menu.setGroupVisible(R.id.group_logout, true);
                menu.setGroupVisible(R.id.group_login, false);
            } else {
                menu.setGroupVisible(R.id.group_logout, false);
                menu.setGroupVisible(R.id.group_login, true);
            }
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_search:
                openSearch();
                break;
            case R.id.action_map:
                Intent intent = new Intent(this, MapActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_share:
                Utilities.getInstance().shareApp(this);
                return true;
            case R.id.action_login:
                ParseLoginBuilder builder = new ParseLoginBuilder(MainActivity.this);
                startActivityForResult(builder.build(), MuslimTravelerConstants.LOGIN_ACTIVITY_CODE);
                break;
            case R.id.action_logout:
                logout();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
