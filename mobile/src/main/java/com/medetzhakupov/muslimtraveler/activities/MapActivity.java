package com.medetzhakupov.muslimtraveler.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.medetzhakupov.muslimtraveler.MuslimTravelerConstants;
import com.medetzhakupov.muslimtraveler.MyApplication;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.Utilities;
import com.medetzhakupov.muslimtraveler.components.CustomClusterManager;
import com.medetzhakupov.muslimtraveler.components.CustomMapFragment;
import com.medetzhakupov.muslimtraveler.components.ParseProxyObject;
import com.medetzhakupov.muslimtraveler.components.PlaceRenderer;
import com.medetzhakupov.muslimtraveler.interfaces.ILastLocationFinder;
import com.medetzhakupov.muslimtraveler.model.Photo;
import com.medetzhakupov.muslimtraveler.model.Place;
import com.medetzhakupov.muslimtraveler.receivers.LocationChangedReceiver;
import com.medetzhakupov.muslimtraveler.receivers.PassiveLocationChangedReceiver;
import com.medetzhakupov.muslimtraveler.utils.GingerbreadLastLocationFinder;
import com.medetzhakupov.muslimtraveler.utils.GingerbreadLocationUpdateRequester;
import com.medetzhakupov.muslimtraveler.utils.LocationUpdateRequester;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class MapActivity extends ActionBarActivity implements ClusterManager.OnClusterClickListener<Place>, ClusterManager.OnClusterInfoWindowClickListener<Place>, ClusterManager.OnClusterItemClickListener<Place>, ClusterManager.OnClusterItemInfoWindowClickListener<Place>{

    protected static String TAG = "MapActivity";

    private final double QIBLA_LATITUDE = 3.0833;
    private final double QIBLA_LONGTUDE = 101.5333;
    private Toolbar mToolbar;
    private FloatingActionButton myLocationButton;
    private Button mBtnSearchThisArea;
    private ProgressBar mProgressSearchThisArea;
    private GoogleMap mMap;
    private CustomClusterManager mClusterManager;

    protected PackageManager packageManager;
    protected NotificationManager notificationManager;
    protected LocationManager locationManager;

    protected Criteria criteria;
    protected ILastLocationFinder lastLocationFinder;
    protected LocationUpdateRequester locationUpdateRequester;
    protected PendingIntent locationListenerPendingIntent;
    protected PendingIntent locationListenerPassivePendingIntent;
    protected Location mLastLocation;

    private int fabY = 0;
    private ParseGeoPoint destination;
    private CustomMapFragment mMapFragment;
    private boolean mMapIsTouched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.title_activity_map);
        mBtnSearchThisArea = (Button) findViewById(R.id.btn_search_this_area);
        mBtnSearchThisArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBtnSearchThisArea.setVisibility(View.GONE);
                mProgressSearchThisArea.setVisibility(View.VISIBLE);
                showFacilitiesNearMe(mMap.getCameraPosition().target);
            }
        });
        mProgressSearchThisArea = (ProgressBar) findViewById(R.id.progress_bar);
        setSupportActionBar(mToolbar);
        setUpMapIfNeeded();
        // Get references to the managers
        packageManager = getPackageManager();
        notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        myLocationButton = (FloatingActionButton)findViewById(R.id.fab_my_location);
        myLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMyLocation();
            }
        });

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        fabY = (int)myLocationButton.getY();

        // Specify the Criteria to use when requesting location updates while the application is Active
        criteria = new Criteria();
        if (MuslimTravelerConstants.USE_GPS_WHEN_ACTIVITY_VISIBLE)
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
        else
            criteria.setPowerRequirement(Criteria.POWER_LOW);

        // Setup the location update Pending Intents
        Intent activeIntent = new Intent(this, LocationChangedReceiver.class);
        locationListenerPendingIntent = PendingIntent.getBroadcast(this, 0, activeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent passiveIntent = new Intent(this, PassiveLocationChangedReceiver.class);
        locationListenerPassivePendingIntent = PendingIntent.getBroadcast(this, 0, passiveIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Instantiate a LastLocationFinder class.
        // This will be used to find the last known location when the application starts.
        lastLocationFinder = new GingerbreadLastLocationFinder(this);
        lastLocationFinder.setChangedLocationListener(oneShotLastLocationUpdateListener);

        // Instantiate a Location Update Requester class based on the available platform version.
        // This will be used to request location updates.
        locationUpdateRequester = new GingerbreadLocationUpdateRequester(locationManager);
    }


    @Override
    protected void onResume() {
        super.onResume();

        // Commit shared preference that says we're in the foreground.
        MyApplication.getSharePreferences().edit().putBoolean(MuslimTravelerConstants.EXTRA_KEY_IN_BACKGROUND, false).apply();

        // Get the last known location (and optionally request location updates) and
        // update the place list.
        boolean followLocationChanges = MyApplication.getSharePreferences().getBoolean(MuslimTravelerConstants.SP_KEY_FOLLOW_LOCATION_CHANGES, true);
        getLocationAndUpdatePlaces(followLocationChanges);
    }

    @Override
    protected void onPause() {
        // Commit shared preference that says we're in the background.
        MyApplication.getSharePreferences().edit().putBoolean(MuslimTravelerConstants.EXTRA_KEY_IN_BACKGROUND, true).apply();

        // Stop listening for location updates when the Activity is inactive.
        disableLocationUpdates();

        super.onPause();
    }

    /**
     * Find the last known location (using a {@link LastLocationFinder}) and updates the
     * place list accordingly.
     * @param updateWhenLocationChanges Request location updates
     */
    protected void getLocationAndUpdatePlaces(boolean updateWhenLocationChanges) {
        // This isn't directly affecting the UI, so put it on a worker thread.
        AsyncTask<Void, Void, Void> findLastLocationTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // Find the last known location, specifying a required accuracy of within the min distance between updates
                // and a required latency of the minimum time required between updates.
                Location lastKnownLocation = lastLocationFinder.getLastBestLocation(MuslimTravelerConstants.MAX_DISTANCE,
                        System.currentTimeMillis()-MuslimTravelerConstants.MAX_TIME);
                showFacilitiesNearMe(lastKnownLocation);
                return null;
            }
        };
        findLastLocationTask.execute();

        // If we have requested location updates, turn them on here.
        toggleUpdatesWhenLocationChanges(updateWhenLocationChanges);
    }

    /**
     * Choose if we should receive location updates.
     * @param updateWhenLocationChanges Request location updates
     */
    protected void toggleUpdatesWhenLocationChanges(boolean updateWhenLocationChanges) {
        // Save the location update status in shared preferences
        MyApplication.getSharePreferences().edit().putBoolean(MuslimTravelerConstants.SP_KEY_FOLLOW_LOCATION_CHANGES, updateWhenLocationChanges).apply();

        // Start or stop listening for location changes
        if (updateWhenLocationChanges)
            requestLocationUpdates();
        else
            disableLocationUpdates();
    }

    /**
     * Start listening for location updates.
     */
    protected void requestLocationUpdates() {
        // Normal updates while activity is visible.
        locationUpdateRequester.requestLocationUpdates(MuslimTravelerConstants.MAX_TIME, MuslimTravelerConstants.MAX_DISTANCE, criteria, locationListenerPendingIntent);

        // Passive location updates from 3rd party apps when the Activity isn't visible.
        locationUpdateRequester.requestPassiveLocationUpdates(MuslimTravelerConstants.PASSIVE_MAX_TIME, MuslimTravelerConstants.PASSIVE_MAX_DISTANCE, locationListenerPassivePendingIntent);

        // Register a receiver that listens for when the provider I'm using has been disabled.
        IntentFilter intentFilter = new IntentFilter(MuslimTravelerConstants.ACTIVE_LOCATION_UPDATE_PROVIDER_DISABLED);
        registerReceiver(locProviderDisabledReceiver, intentFilter);

        // Register a receiver that listens for when a better provider than I'm using becomes available.
        String bestProvider = locationManager.getBestProvider(criteria, false);
        String bestAvailableProvider = locationManager.getBestProvider(criteria, true);
        if (bestProvider != null && !bestProvider.equals(bestAvailableProvider)) {
            locationManager.requestLocationUpdates(bestProvider, 0, 0, bestInactiveLocationProviderListener, getMainLooper());
        }
    }

    /**
     * Stop listening for location updates
     */
    protected void disableLocationUpdates() {
        unregisterReceiver(locProviderDisabledReceiver);
        locationManager.removeUpdates(locationListenerPendingIntent);
        locationManager.removeUpdates(bestInactiveLocationProviderListener);
        if (isFinishing())
            lastLocationFinder.cancel();
        if (MuslimTravelerConstants.DISABLE_PASSIVE_LOCATION_WHEN_USER_EXIT && isFinishing())
            locationManager.removeUpdates(locationListenerPassivePendingIntent);
    }

    /**
     * One-off location listener that receives updates from the {@link LastLocationFinder}.
     * This is triggered where the last known location is outside the bounds of our maximum
     * distance and latency.
     */
    protected LocationListener oneShotLastLocationUpdateListener = new LocationListener() {
        public void onLocationChanged(Location l) {
            updatePlaces(l, MuslimTravelerConstants.DEFAULT_RADIUS, true);
        }

        public void onProviderDisabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
        public void onProviderEnabled(String provider) {}
    };

    /**
     * If the best Location Provider (usually GPS) is not available when we request location
     * updates, this listener will be notified if / when it becomes available. It calls
     * requestLocationUpdates to re-register the location listeners using the better Location
     * Provider.
     */
    protected LocationListener bestInactiveLocationProviderListener = new LocationListener() {
        public void onLocationChanged(Location l) {}
        public void onProviderDisabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
        public void onProviderEnabled(String provider) {
            // Re-register the location listeners using the better Location Provider.
            requestLocationUpdates();
        }
    };

    /**
     * If the Location Provider we're using to receive location updates is disabled while the
     * app is running, this Receiver will be notified, allowing us to re-register our Location
     * Receivers using the best available Location Provider is still available.
     */
    protected BroadcastReceiver locProviderDisabledReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean providerDisabled = !intent.getBooleanExtra(LocationManager.KEY_PROVIDER_ENABLED, false);
            // Re-register the location listeners using the best available Location Provider.
            if (providerDisabled)
                requestLocationUpdates();
        }
    };

    protected void updatePlaces(Location location, int radius, boolean forceRefresh) {
        if (location != null) {
            Log.d(TAG, "Updating place list.");
            showFacilitiesNearMe(location);
        }
        else
            Log.d(TAG, "Updating place list for: No Previous Location Found");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_search:
                return true;
            case R.id.action_navigation:
                startNavigationIntent();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    private void moveFab(float toTranslationY) {
        if (ViewHelper.getTranslationY(myLocationButton) == toTranslationY) {
            return;
        }
        ValueAnimator animator = ValueAnimator.ofFloat(ViewHelper.getTranslationY(myLocationButton), toTranslationY).setDuration(200);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float translationY = Float.valueOf(animation.getAnimatedValue().toString());
                ViewHelper.setTranslationY(myLocationButton, translationY);
            }
        });
        animator.start();
    }

    public void showFab() {
        moveFab(fabY);
    }

    public void hideFab() {
        moveFab(fabY + 300);
    }

    private void startNavigationIntent(){
        try{
            if (destination != null) {
                Location location = Utilities.getInstance().getLastBestLocation(MuslimTravelerConstants.MAX_DISTANCE,
                        MuslimTravelerConstants.MAX_TIME);
                String directionUri = String.format("http://maps.google.com/maps?saddr=%s,%s&daddr=%s,%s",
                        location.getLatitude(),
                        location.getLongitude(),
                        destination.getLatitude(), destination.getLongitude());
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse(directionUri));
                startActivity(intent);
            } else {
                Toast.makeText(this, getString(R.string.select_destination), Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){}
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMapFragment = ((CustomMapFragment)getSupportFragmentManager().findFragmentById(R.id.map));
            mMapFragment.setTouchListener(new CustomMapFragment.ITouchListener() {
                @Override
                public void onTouch(MotionEvent ev) {
                    switch (ev.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            mMapIsTouched = true;
                            mBtnSearchThisArea.setVisibility(View.GONE);
                            break;
                        case MotionEvent.ACTION_UP:
                            mMapIsTouched = false;
//                            if(calculationByDistance(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()), mMap.getCameraPosition().target) > 100){
                                mBtnSearchThisArea.setVisibility(View.VISIBLE);
//                            }
                            break;
                    }
                }
            });
            mMap = mMapFragment.getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                showFab();
            }
        });
        getMyLocation();

        mClusterManager = new CustomClusterManager(this, mMap, new CustomClusterManager.ICameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
//                if (!mMapIsTouched){
                if (!mBtnSearchThisArea.isShown())
                    mBtnSearchThisArea.setVisibility(View.VISIBLE);
//                    showFacilitiesNearMe(mMap.getCameraPosition().target);
//                }
            }
        });
        mClusterManager.setRenderer(new PlaceRenderer(this, mMap, mClusterManager));
        mMap.setOnCameraChangeListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);
    }

    private void getMyLocation() {
        try{
            mLastLocation = lastLocationFinder.getLastBestLocation(MuslimTravelerConstants.MAX_DISTANCE, MuslimTravelerConstants.MAX_TIME);
            CameraUpdate update = Utilities.getInstance().getCameraUpdateWithGivenLocation(mLastLocation);
            if (update != null) {
                mMap.animateCamera(update);
            }
            showFacilitiesNearMe(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
        }catch(Exception ex){
            System.out.println(ex.toString());
        }
    }

    private void showFacilitiesNearMe(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        showFacilitiesNearMe(latLng);
    }

    private void showFacilitiesNearMe(LatLng location)
    {
        ParseQuery<Place> query = new ParseQuery<Place>("Place");
        query.whereNear("location", new ParseGeoPoint(location.latitude, location.longitude));
        query.include("category");
        query.include("photos");
        query.findInBackground(new FindCallback<Place>() {
            @Override
            public void done(List<Place> list, ParseException e) {
                if (e == null) {
                    mClusterManager.clearItems();
                    mClusterManager.addItems(list);
                    mClusterManager.cluster();
                }
                if (mProgressSearchThisArea != null && mProgressSearchThisArea.isShown())
                    mProgressSearchThisArea.setVisibility(View.GONE);
                mBtnSearchThisArea.setVisibility(View.GONE);
            }
        });

    }

    public double calculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec);

        return Radius * c;
    }

    @Override
    public boolean onClusterClick(Cluster<Place> cluster) {
        hideFab();
        return false;
    }

    @Override
    public boolean onClusterItemClick(Place place) {
        destination = place.getLocation();
        hideFab();
        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Place> cluster) {

    }

    @Override
    public void onClusterItemInfoWindowClick(Place place) {
        ArrayList<String> urlList = new ArrayList<String>();
        for (Photo item:place.getPhotos()){
            urlList.add(item.getPhotoFile().getUrl());
        }

        ParseProxyObject ppo = new ParseProxyObject(place);
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putStringArrayListExtra("photos", urlList);
        intent.putExtra("parseObject", ppo);
        startActivity(intent);
    }
}
