package com.medetzhakupov.muslimtraveler.activities;

import android.animation.ValueAnimator;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

import com.dd.CircularProgressButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.medetzhakupov.muslimtraveler.adapters.CategoryAdapter;
import com.medetzhakupov.muslimtraveler.model.FullAddress;
import com.medetzhakupov.muslimtraveler.adapters.HorizontalRecyclerAdapter;
import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.Utilities;
import com.medetzhakupov.muslimtraveler.model.Category;
import com.medetzhakupov.muslimtraveler.model.Photo;
import com.medetzhakupov.muslimtraveler.model.Place;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import at.markushi.ui.CircleButton;

/**
 * Created by Medet Zhakupov on 10/4/2015.
 */
public class CreatePlaceActivity extends ActionBarActivity implements View.OnClickListener, SlidingUpPanelLayout.PanelSlideListener, AdapterView.OnItemSelectedListener {
    private final String FULL_ADDRESS = "fullAddress";
    private final String TITLE = "title";
    private final String CATEGORY = "category";
    private final String PHONE = "phone";
    private final String DESCRIPTION = "description";

    private static final String TAG = "CreatePlaceActivity";
    private SlidingUpPanelLayout mSlidingUpPanelLayout;

    private MapFragment mMapFragment;

    private GoogleMap mMap;
    ArrayList<Photo> mPhotoObjects;
    FullAddress fullAddress;
    Category selectedCategory;

    PopupWindow popupWindowAddress;
    private Toolbar mToolbar;
    private EditText mAddress, mTitle, mPhoneNumber, mLink, mDescription;
    private Spinner mCategory;
    private RecyclerView mRecyclerView;
    private HorizontalRecyclerAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private CircularProgressButton btnSubmit;
    private CircleButton btnPopupDone;
    private String errorText = "";
    private ParseFile mapSnapshotFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_place);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.title_activity_create);
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setUpPopupWindow();
        btnSubmit = (CircularProgressButton) findViewById(R.id.btn_submit);

        btnPopupDone.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        //mBtnSubmit.setIndeterminateProgressMode(true);

        mTitle = (EditText)findViewById(R.id.edit_text_title);
        mPhoneNumber = (EditText)findViewById(R.id.edit_text_phone);
        mLink = (EditText)findViewById(R.id.edit_text_link);
        mDescription = (EditText)findViewById(R.id.edit_text_des);

        mCategory = (Spinner)findViewById(R.id.spinner_category);

        if (Utilities.getInstance().isInternetOn() && savedInstanceState == null){
            Category.getQuery().findInBackground(new FindCallback<Category>() {
                @Override
                public void done(List<Category> categories, ParseException e) {
                    if (e == null) {
                        initCategorySpinner(categories);
                        for (Category item:categories){
                            item.pinInBackground();
                        }
                    } else {

                    }
                }
            });
        } else {
            Category.getQuery().fromLocalDatastore().findInBackground(new FindCallback<Category>() {
                @Override
                public void done(List<Category> categories, ParseException e) {
                    if (categories != null && categories.size() > 0)
                        initCategorySpinner(categories);
                }
            });
        }

        mCategory.setOnItemSelectedListener(this);

        mSlidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        mSlidingUpPanelLayout.setPanelSlideListener(this);

        mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new HorizontalRecyclerAdapter(this, null, new HorizontalRecyclerAdapter.IAdapterItemClickListener() {
            @Override
            public void onItemClick(int viewId, int position) {
                switch (viewId){
                    case R.id.image_view:
                        if (mAdapter.getData().size() > position){
                            Intent intent = new Intent(getApplicationContext(), FullscreenActivity.class);
                            intent.putStringArrayListExtra("URLs", mAdapter.getData());
                            intent.putExtra("position", position);
                            startActivity(intent);
                        } else {
                            imageChoose();
                        }
                        break;
                    case R.id.delete_icon:
                        deleteImage(position);
                        break;
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        mMapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mapContainer, mMapFragment, "map");
        fragmentTransaction.commit();

        setUpMapIfNeeded();

        if (savedInstanceState != null){
            fullAddress = (FullAddress)savedInstanceState.getSerializable(FULL_ADDRESS);
            mTitle.setText(savedInstanceState.getString(TITLE, ""));
            int position = mCategory.getAdapter() != null ?
                    ((CategoryAdapter) mCategory.getAdapter()).getSelectedItemPosition(savedInstanceState.getString(CATEGORY, ""))
                    : 0;
            mCategory.setSelection(position);
            mPhoneNumber.setText(savedInstanceState.getString(PHONE, ""));
            mDescription.setText(savedInstanceState.getString(DESCRIPTION, ""));
        }
    }

    void setUpPopupWindow()
    {
        popupWindowAddress = new PopupWindow(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        View popupView = getLayoutInflater().inflate(R.layout.popup_layout, null);
        popupWindowAddress .setContentView(popupView);
        popupWindowAddress.setBackgroundDrawable(new BitmapDrawable());
        popupWindowAddress.setFocusable(true);
        popupWindowAddress.setOutsideTouchable(true);
        mAddress = (EditText)popupView.findViewById(R.id.edit_text_address);
        btnPopupDone = (CircleButton)popupView.findViewById(R.id.btn_done);
    }

    void initCategorySpinner(List<Category> categories)
    {
        // Create an ArrayAdapter using the string array and a default spinner layout
        CategoryAdapter adapter = new CategoryAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, categories);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mCategory.setAdapter(adapter);
    }

    void deleteImage(final int position)
    {
        Utilities.getInstance().alertDeleteConfirmDialog(this, new Utilities.ICallback() {
            @Override
            public void onCallback() {
                mAdapter.deleteData(position);
                mPhotoObjects.get(position).deleteInBackground(new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {

                        }
                    }
                });
            }
        }).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(FULL_ADDRESS, fullAddress);
        outState.putString(TITLE, mTitle.getText().toString());
        outState.putString(CATEGORY, selectedCategory != null ? selectedCategory.getName() : "");
        outState.putString(PHONE, mPhoneNumber.getText().toString());
        outState.putString(DESCRIPTION, mDescription.getText().toString());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_done:
                dismissPopup();
                mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                break;
            case R.id.btn_submit:
                if (ParseUser.getCurrentUser() == null) {
                    Toast.makeText(this, getString(R.string.only_signed_users_can_create_place), Toast.LENGTH_SHORT).show();
                    break;
                }

                fullAddress.setAddress(mAddress.getText().toString());
                if(isValid()){
                    if (btnSubmit.getProgress() == 0) {
                        //simulateProgress(btnSubmit);
                        saveNewPlace(0, mapSnapshotFile);
                    } else {
                        btnSubmit.setProgress(0);
                    }
                } else if (Utilities.getInstance().validateStringForNullAndEmpty(errorText)){
                    Toast.makeText(this, errorText, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // In case Google Play services has since become available.
        setUpMapIfNeeded();
    }

    @Override
    public void onBackPressed() {

        if (mSlidingUpPanelLayout != null &&
                (mSlidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mSlidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } if(popupWindowAddress != null && popupWindowAddress.isShowing()){
            popupWindowAddress.dismiss();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_create_place, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) CreatePlaceActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(CreatePlaceActivity.this.getComponentName()));
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_search){

        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri imageUri = null;
        String filePath = "";
        if (resultCode == RESULT_OK && requestCode == 0) {
            try {
                if (data != null) {
                    imageUri = data.getData();
                    String[] projection = { MediaStore.Images.Media.DATA };
                    Cursor cursor = managedQuery(imageUri, projection, null,
                            null, null);
                    int column_index = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToFirst();
                    filePath = cursor.getString(column_index);
                    System.out.println("the file path data is not null"+filePath);
                    saveScaledPhoto(filePath);
                }
            } catch (Exception ex) {
                Log.e(TAG, ex.toString());
                ex.printStackTrace();
            }
        }
    }

    private boolean isValid()
    {
        boolean valid = false;
        if(Utilities.getInstance().validateEditText(mTitle) &&
                Utilities.getInstance().validateEditText(mDescription)) {
            if (fullAddress != null){
                if (mPhotoObjects != null && mPhotoObjects.size() > 0){
                    valid = true;
                } else {
                    valid = false;
                    errorText = "At least one image is required.";
                }

                if (fullAddress.getAddress().isEmpty()){
                    valid = false;
                    errorText = "Address is required!";
                }
            }else if (fullAddress == null){
                valid = false;
                errorText = "Location is required!";
            }
        }

        return valid;
    }

    private void saveNewPlace(final int i, final ParseFile parseFile) {
            mPhotoObjects.get(i).getPhotoFile().saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    try {
                        if (i < mPhotoObjects.size() - 1) {
                            saveNewPlace(i + 1, parseFile);
                        } else if (i == mPhotoObjects.size() - 1){
                            final Place newPlace = new Place();
                            newPlace.setIsPublished(true);
                            newPlace.setTitle(String.valueOf(mTitle.getText()));
                            newPlace.setAddress(fullAddress.getAddress());
                            newPlace.setCity(fullAddress.getCity());
                            newPlace.setCountry(fullAddress.getCountry());
                            newPlace.setLocation(new ParseGeoPoint(fullAddress.getLatitude(), fullAddress.getLongitude()));
                            newPlace.setCategory(selectedCategory);
                            newPlace.setPhone(String.valueOf(mPhoneNumber.getText()));
                            newPlace.setMapSnapshot(parseFile);
                            newPlace.setPhotos(mPhotoObjects);
                            newPlace.setDescription(String.valueOf(mDescription.getText()));
                            newPlace.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null){
                                        ParseUser user = ParseUser.getCurrentUser();
                                        ParseRelation<Place> relation = user.getRelation("places");
                                        relation.add(newPlace);
                                        user.saveInBackground();
                                        btnSubmit.setProgress(100);
                                        Utilities.getInstance().alertConfirmDialog(mSlidingUpPanelLayout.getContext(), "Create Place", "Your new place successfully submitted and will be published within one day!", new Utilities.ICallback() {
                                            @Override
                                            public void onCallback() {
                                                resetForm();
                                                mSlidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                                                btnSubmit.setProgress(0);
                                                btnSubmit.refreshDrawableState();
                                            }
                                        }).show();
                                    } else {
                                        btnSubmit.setProgress(-1);
                                    }
                                }
                            });
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            },new ProgressCallback() {
                @Override
                public void done(Integer integer) {
                    int currentProgress = 100/mPhotoObjects.size() * i;
                    int prg = currentProgress + integer/mPhotoObjects.size();
                    if (prg < 95){
                        btnSubmit.setProgress(prg);
                    }
                }
            });
    }

    private void resetForm()
    {
        mTitle.setText("");
        mPhoneNumber.setText("");
        mLink.setText("");
        mDescription.setText("");
        mMap.clear();
        mAdapter.reset();
        mPhotoObjects.clear();
        fullAddress = null;
    }

    private void simulateProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                if (value < 99){
                    button.setProgress(value);
                }
                //button.setProgress(-1); to simulate error progress
            }
        });
        widthAnimation.start();
    }

    void imageChoose() {
        try {
            startActivityForResult(Utilities.getInstance().makePhotoIntent("Choose an action", this), 0);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = mMapFragment.getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                CameraUpdate update = Utilities.getInstance().getCameraUpdateWithLastKnownLocation();
                if (update != null) {
                    mMap.moveCamera(update);
                }

                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        fullAddress = Utilities.getInstance().getAddress(latLng.latitude, latLng.longitude);
                        mMap.clear();
                        mMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title(fullAddress.getAddress()));
                        showPopup();
                        if (fullAddress.toString() != null && !fullAddress.toString().isEmpty()) {
                            mAddress.setText(fullAddress.getAddress());
                        }
                    }
                });

                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        showPopup();
                        mAddress.setText(fullAddress.getAddress());
                    }
                });
            }
        }
    }

    @Override
    public void onPanelSlide(View panel, float slideOffset) {
        Log.i(TAG, "onPanelSlide, offset " + slideOffset);
    }

    @Override
    public void onPanelExpanded(View panel) {
        Log.i(TAG, "onPanelExpanded");
        dismissPopup();
        if (fullAddress != null){
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(fullAddress.getLatitude(), fullAddress.getLongitude()), 17f);
            mMap.moveCamera(cameraUpdate);
            mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                @Override
                public void onSnapshotReady(Bitmap bitmap) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
                    byte[] byteArray = stream.toByteArray();
                    String guid = String.format("%s.png",java.util.UUID.nameUUIDFromBytes(byteArray));
                    if (mapSnapshotFile != null)
                        mapSnapshotFile = null;
                    mapSnapshotFile = new ParseFile(guid, byteArray);
                }
            });
        }
    }

    @Override
    public void onPanelCollapsed(View panel) {
        Log.i(TAG, "onPanelCollapsed");
        if (fullAddress == null){
            CameraUpdate update = Utilities.getInstance().getCameraUpdateWithLastKnownLocation();
            if (update != null) {
                mMap.moveCamera(update);
            }
        }
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15f), 1000, null);
    }

    @Override
    public void onPanelAnchored(View panel) {
        Log.i(TAG, "onPanelAnchored");
    }

    @Override
    public void onPanelHidden(View panel) {
        Log.i(TAG, "onPanelHidden");
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        selectedCategory = (Category)adapterView.getAdapter().getItem(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void showPopup()
    {
        if(popupWindowAddress != null && !popupWindowAddress.isShowing()){
            popupWindowAddress.showAsDropDown(mToolbar);
        }
    }

    private void dismissPopup()
    {
        if(popupWindowAddress != null && popupWindowAddress.isShowing()){
            popupWindowAddress.dismiss();
        }
    }

    private void saveScaledPhoto(final String filePath) {
         try{
             mAdapter.setData(filePath);
             Bitmap imageScaled  = Utilities.getInstance().decodeScaledBitmapFromSdCard(filePath, 500, 500);
             ByteArrayOutputStream bos = new ByteArrayOutputStream();
             //rotatedScaledMealImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
             imageScaled.compress(Bitmap.CompressFormat.JPEG, 100, bos);

             byte[] scaledData = bos.toByteArray();
             imageScaled.recycle();

             // Save the scaled image to Parse
             String filename= filePath.substring(filePath.lastIndexOf("/")+1);
             final ParseFile photoFile = new ParseFile( filename, scaledData);
             addPhotoToPhotoObject(photoFile);
         }catch (Exception ex){
             Log.e(TAG, ex.toString());
         }
    }

    public void addPhotoToPhotoObject(ParseFile photoFile){
        if(mPhotoObjects == null)
            mPhotoObjects = new ArrayList<>();
        final Photo photo = new Photo();
        photo.setPhotoFile(photoFile);
        mPhotoObjects.add(photo);
//        photo.saveInBackground(new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//                mPhotoObjects.add(photo);
//            }
//        });
    }
}
