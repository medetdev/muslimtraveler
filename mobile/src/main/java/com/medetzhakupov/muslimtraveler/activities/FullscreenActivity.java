package com.medetzhakupov.muslimtraveler.activities;

import com.medetzhakupov.muslimtraveler.MyApplication;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.medetzhakupov.muslimtraveler.R;
import com.medetzhakupov.muslimtraveler.model.Photo;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.ArrayList;
import com.medetzhakupov.muslimtraveler.activities.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;
    private ArrayList<String> photosURL;
    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActionBar().hide();
        setContentView(R.layout.activity_fullscreen);

        if (savedInstanceState != null){
            photosURL = savedInstanceState.getStringArrayList("URLs");
            position = savedInstanceState.getInt("position");
        } else{
            photosURL = getIntent().getStringArrayListExtra("URLs");
            position = getIntent().getIntExtra("position", 0);
        }

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(photosURL);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(position, true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("URLs", photosURL);
        outState.putInt("position", position);
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends PagerAdapter {
        private ArrayList<String> mPhotos;

        ScreenSlidePagerAdapter(ArrayList<String> photos){
            mPhotos = photos;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            //View view = LayoutInflater.from(getApplicationContext()).inflate()

            ImageView imageView = new ImageView(getApplicationContext());
            ViewGroup.LayoutParams imageParams = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(imageParams);

            ImageLoader.getInstance()
                    .displayImage("file:///" + mPhotos.get(position),
                            imageView,
                            MyApplication.options);
            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            if (mPhotos != null)
                return  mPhotos.size();
            return 0;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }
    }
}
