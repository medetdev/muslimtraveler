package com.medetzhakupov.muslimtraveler;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.medetzhakupov.muslimtraveler.model.Bookmark;
import com.medetzhakupov.muslimtraveler.model.ErrorReport;
import com.medetzhakupov.muslimtraveler.model.FullAddress;
import com.medetzhakupov.muslimtraveler.model.Place;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Medet Zhakupov on 15/4/2015.
 */
public class Utilities {
    private static Utilities utilities;

    public static Utilities getInstance()
    {
        if (utilities == null){
            utilities = new Utilities();
        }
        return utilities;
    }

    public FullAddress getAddress(double latitude, double longitude) {
        FullAddress fullAddress = new FullAddress();
        try {
            fullAddress.setLatitude(latitude);
            fullAddress.setLongitude(longitude);

            Geocoder geocoder = new Geocoder(MyApplication.getContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder addressBuilder = new StringBuilder();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++){
                    if (i == address.getMaxAddressLineIndex() - 1)
                        addressBuilder.append(address.getAddressLine(i));
                    else
                        addressBuilder.append(address.getAddressLine(i) + " , ");
                }

                String exactAddress = addressBuilder.toString().trim();
                String city = address.getLocality();
                String country = address.getCountryName();

                fullAddress.setAddress(exactAddress);
                fullAddress.setLocality(address.getLocality());
                fullAddress.setCity(city);
                fullAddress.setCountry(country);
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return fullAddress;
    }

    public Intent makePhotoIntent(String title, Context ctx) throws IOException {

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        galleryIntent.setType("image/*");
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);

        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, title);


            Intent cameraIntent = new Intent(
                    android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            String mTempImage = null;
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            mTempImage = ts + ".jpg";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, mTempImage);
            Uri imageUri = ctx.getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            if (mTempImage != null) {
                Intent[] intentArray = { cameraIntent };
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);

                cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
                        imageUri); // add file uri (photo is saved here)
                cameraIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                Intent[] extraIntents = { cameraIntent };
                chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
            }
        return chooser;
    }

    public Bitmap decodeScaledBitmapFromSdCard(String filePath,
                                                      int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    public int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;

//        Bitmap imageScaled = Bitmap.createScaledBitmap(image, 500, 500
//                     * image.getHeight() / image.getWidth(), false);

        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public CameraUpdate getCameraUpdateWithGivenLocation(Location loc) {
        if (loc != null) {
            return CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 14.0f));
        }
        return null;
    }

    public CameraUpdate getCameraUpdateWithLastKnownLocation() {
        Location loc = getLastBestLocation(MuslimTravelerConstants.MAX_DISTANCE, MuslimTravelerConstants.MAX_TIME);
        if (loc != null) {
            return CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 14.0f));
        }
        return null;
    }

    public Location getLastBestLocation(int minDistance, long minTime) {
        LocationManager lm = (LocationManager) MyApplication.getContext().getSystemService(Context.LOCATION_SERVICE);
        Location bestResult = null;
        float bestAccuracy = Float.MAX_VALUE;
        long bestTime = Long.MIN_VALUE;

        // Iterate through all the providers on the system, keeping
        // note of the most accurate result within the acceptable time limit.
        // If no result is found within maxTime, return the newest Location.
        List<String> matchingProviders = lm.getAllProviders();
        for (String provider: matchingProviders) {
            Location location = lm.getLastKnownLocation(provider);
            if (location != null) {
                float accuracy = location.getAccuracy();
                long time = location.getTime();

                if ((time > minTime && accuracy < bestAccuracy)) {
                    bestResult = location;
                    bestAccuracy = accuracy;
                    bestTime = time;
                }
                else if (time < minTime && bestAccuracy == Float.MAX_VALUE && time > bestTime) {
                    bestResult = location;
                    bestTime = time;
                }
            }
        }

        return bestResult;
    }



    public boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)MyApplication.getContext().getSystemService(MyApplication.getContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(MyApplication.getContext(), " Not Connected ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

    public AlertDialog alertDeleteConfirmDialog(Context context, final ICallback callback)
    {
        AlertDialog alertDialog =new AlertDialog.Builder(context)
                //set message, title, and icon
                .setTitle("Delete")
                .setMessage("Do you want to delete ?")
                .setIcon(R.drawable.ic_delete_black_24dp)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        callback.onCallback();
                        dialog.dismiss();
                    }

                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return alertDialog;

    }

    public AlertDialog alertConfirmDialog(Context context, String title, String content, final ICallback callback)
    {
        AlertDialog alertDialog =new AlertDialog.Builder(context)
                //set message, title, and icon
                .setTitle(title)
                .setMessage(content)
                .setIcon(R.drawable.ic_info_black_24dp)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        callback.onCallback();
                        dialog.dismiss();
                    }
                }).create();
        return alertDialog;

    }

    public boolean validateEditText(EditText editText)
    {
        boolean isValid = true;
        if (editText.getText().toString() != null &&
                !editText.getText().toString().isEmpty()){
            isValid = true;
            editText.setError(null);
        } else {
            editText.setError("This field is required");
            isValid = false;
        }

        return isValid;
    }

    public boolean validateStringForNullAndEmpty(String str){
        return str != null && !str.isEmpty();
    }

    public void saveBookmark(String objectId)
    {
        saveBookmark(objectId, null);
    }

    public void saveBookmark(final String objectId, final SaveCallback callback)
    {
        try {
            final ParseUser user = ParseUser.getCurrentUser();
            final ParseRelation<Place> relation = user.getRelation("bookmarks");
            Place.getQuery().getInBackground(objectId, new GetCallback<Place>() {
                @Override
                public void done(Place place, ParseException e) {
                    relation.add(place);
                    user.saveInBackground(callback);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void shareApp(Context context)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Here is the Muslim traveler app for all muslims travelers around the world. I'm using it. Try it out.";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Muslim Traveler");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public void shareContent(Context context, String content)
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Muslim Traveler");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public void showToast(Context context, int resource){
        Toast.makeText(context, resource, Toast.LENGTH_SHORT).show();
    }

    public void showToast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public interface ICallback {
        public void onCallback();
    }
}
