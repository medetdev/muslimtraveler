package com.medetzhakupov.muslimtraveler;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import com.medetzhakupov.muslimtraveler.model.Bookmark;
import com.medetzhakupov.muslimtraveler.model.Category;
import com.medetzhakupov.muslimtraveler.model.ErrorReport;
import com.medetzhakupov.muslimtraveler.model.Feedback;
import com.medetzhakupov.muslimtraveler.model.Photo;
import com.medetzhakupov.muslimtraveler.model.Place;
import com.medetzhakupov.muslimtraveler.model.Rating;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.Parse;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseTwitterUtils;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medet Zhakupov on 6/4/2015.
 */
public class MyApplication extends Application {
    static Context mContext;
    public static DisplayImageOptions options;

    @Override
    public void onCreate() {
        super.onCreate();

        // Enable Crash Reporting
        ParseCrashReporting.enable(this);
        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(ErrorReport.class);
        ParseObject.registerSubclass(Category.class);
        ParseObject.registerSubclass(Place.class);
        ParseObject.registerSubclass(Photo.class);
        ParseObject.registerSubclass(Rating.class);
        ParseObject.registerSubclass(Bookmark.class);
        ParseObject.registerSubclass(Feedback.class);
        Parse.initialize(this, "PbvTxq6t3Xbigv5dkKchAqDjbMi1FcZCTbC6drVi", "lCcnOsYfz2ubNPr3loqlenAVlRUlZRbwZZQWxG3d");
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.color.white)
                .displayer(new FadeInBitmapDisplayer(500))
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
//        List<Category> categories = new ArrayList<>();
//        String[] names = {"Prayer", "Food", "Outlet", "Other"};
//        for (String item:names){
//             Category category = new Category();
//             category.setName(item);
//             categories.add(category);
//        }
//
//        ParseObject.saveAllInBackground(categories, new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//                if (e == null){
//
//                }
//            }
//        });
        mContext = getApplicationContext();
        ParseFacebookUtils.initialize(this);
        // Optional - If you don't want to allow Twitter login, you can
        // remove this line (and other related ParseTwitterUtils calls)
        ParseTwitterUtils.initialize(getString(R.string.twitter_consumer_key),
                getString(R.string.twitter_consumer_secret));
        initImageLoader(mContext);
    }

    public static Context getContext()
    {
        return mContext;
    }

    public static SharedPreferences getSharePreferences()
    {
        return getContext().getSharedPreferences("MuslimTraveler",MODE_PRIVATE);
    }

    public void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }
}
